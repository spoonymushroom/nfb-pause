using UnityEngine;
using System.Collections;

public class MainController : MonoBehaviour {
	
	public string language = "text_fr_ca";
	
	DialogController dialogController;

	// Use this for initialization
	void Start () {
		dialogController = GetComponent<DialogController>();
		// TODO: use current system language
//		switch (System.Globalization.CultureInfo.CurrentCulture.TwoLetterISOLanguageName) {
//		case "en":
//			language = "text_en";
//			break;
//		case "fr":
//			language = "text_fr_ca";
//			break;
//		default:
//			language = "text_fr_ca";
//			break;
//		}
//		Localization.instance.currentLanguage = language;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	// Testing UI
	void OnGUI () {
		if (GUI.Button(new Rect(10, 10, 150, 60), "Reset vignette"))
		{
			
			dialogController.state = DialogController.DialogState.Idle;
		
			//dialogController.GetComponent<DialogController>().SayLine("greeting");
		}
	}
}
