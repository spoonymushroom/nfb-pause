using UnityEngine;
using System.Collections.Generic;
using System.Linq;

[RequireComponent(typeof(UIInput))]
public class TextInput : MonoBehaviour {
	
	public event System.Action<string> TextSubmitted;	
	public ResponseButton okButton;
	
//	public string Dictionary { get; set; }
		
	DataStore db;
	UIInput input;

	// Use this for initialization
	void Start () {
		db = GameObject.Find("MainController").GetComponent<DataStore>();
		input = GetComponent<UIInput>();
//		okButton = transform.parent.GetComponentInChildren<ResponseButton>();
		okButton.Click += OnSubmit;
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	void OnSubmit(string text) {
		if (TextSubmitted != null) {
			TextSubmitted(text);
		}
	}
}
