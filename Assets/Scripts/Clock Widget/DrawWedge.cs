using UnityEngine;
using System.Collections;

public class DrawWedge : MonoBehaviour {
	
	public float angle = 0;	
	private Mesh mesh;

	// Use this for initialization
	void Start () {
		mesh = new Mesh();
		GetComponent<MeshFilter>().mesh = mesh;
	}
	
	// Update is called once per frame
	void Update () {
		mesh.Clear();
		
		int numTriangles = (int)(angle / 10);
		int numVertices = numTriangles + 2;
		Vector3[] vertices = new Vector3[numVertices];
		int[] triangles = new int[numTriangles * 3];
		vertices[0] = Vector3.zero;
		
		float angleStep = angle / numTriangles;
		Vector3 startPoint = Vector3.forward;
		
		for (int i = 0; i <= numTriangles; i++) {			
			Quaternion rot = Quaternion.AngleAxis(angleStep * i, Vector3.up);
			vertices[i + 1] = rot * startPoint;
		}
		
		for (int i = 0; i < numTriangles; i++) {
			triangles[i * 3] = 0;
			triangles[i * 3 + 1] = i + 1;
			triangles[i * 3 + 2] = i + 2;
		}
		
		mesh.vertices = vertices;
		mesh.triangles = triangles;
	}
}
