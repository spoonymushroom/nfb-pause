using UnityEngine;
using System.Collections;

public class DialTime : MonoBehaviour {
	
	public event System.Action timeChanged;
	
	// Object references
	public Transform pivot;
	public GameObject clockFace;
	public DrawWedge wedge;
	public int minutes = 0;
	
	private bool isDragging = false;
	
	public void Reset() {
		pivot.localRotation = Quaternion.identity;
		minutes = 0;
		wedge.angle = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (isDragging) {
			Vector3 clockScreenCentre = Camera.main.WorldToScreenPoint(pivot.position);
			Vector3 newDir = Input.mousePosition - clockScreenCentre;
			float angle = Mathf.Rad2Deg * Mathf.Atan2(newDir.x, newDir.y);
			if (angle < 0) angle += 360;
			
			pivot.localRotation = Quaternion.AngleAxis(angle, Vector3.up);
			
			minutes = (int)(angle / 360f * 60);
			
			wedge.angle = angle;
		}
	}
	
	void OnMouseDown() {
		gameObject.renderer.material.color = new Color(1f, 1f, 0f, 1f);
		isDragging = true;
	}
	
	void OnMouseUp() {
		gameObject.renderer.material.color = new Color(1f, 1f, 1f, 1f);
		isDragging = false;
		
		if (timeChanged != null)
			timeChanged();
	}
}
