using UnityEngine;
using System;
using System.Collections.Generic;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;
using Random = UnityEngine.Random;

using System.Text;

[RequireComponent(typeof(UILabel))]
public class TextAnimate : MonoBehaviour
{
	//Text materializes character by character. How long between each
	//character pops in?
	public float characterDelay = 0.067f;
	//How egregious should the easing effect be?
	public float bounceTime = 0.6f;

	//Index of how far along the string we are in terms of character pop-in.
	int readHead = 0;
	//Current timeout until next pop-in
	float curCharacterDelay = 0;
	//Timer for each character since it popped-in, used for easing
	float[] timeSinceCharCreation;
	
	// Localization key
	// public string key;

	Waiter w;
	
	// UI element that actually draws the text
	UILabel label;
	
	// The localized string
	public string text = "nothing nothing";
	
	// TODO: Reimplement some kind of "bounce in" within the NGUI framework.
	// We can't change the size of position of individual letters.

	void Start()
	{
		timeSinceCharCreation = new float[text.Length];
		label = GetComponent<UILabel>();
	}
	
	public void Say(string textToDisplay) {
		readHead = 0;
		text = textToDisplay;
		timeSinceCharCreation = new float[text.Length];
		// var ca = GameObject.Find("Creature").GetComponent<CreatureAnimator>();
		// ca.TalkingState = TalkingState.Speaking;
		if (w != null)
			Destroy(w);
		w = Waiters.DoUntil( _ => readHead >= text.Length - 1,
			_ => {});
			//.Then( () => ca.TalkingState = TalkingState.Idle );
	}
	
	void Update()
	{
		//Debug to reset progress through current text box.
		if(Input.GetKeyDown("r"))
		{
			readHead = 0;
			timeSinceCharCreation = new float[text.Length];

			if(w != null)
				Destroy(w);

			// var ca = GameObject.Find("Creature").GetComponent<CreatureAnimator>();
			//ca.TalkingState = TalkingState.Speaking;
			w = Waiters.DoUntil( _ => readHead >= text.Length - 1,
				_ => {});
			//	.Then( () => ca.TalkingState = TalkingState.Idle );
		}

		//Increment shader gradient scroll effect.
		//textMat.SetFloat("_Phase", Time.time);

		//If the read head isn't at the end of the text, tick down the delay
		//to advance it. And if we're holding space, advance it even faster.
		if(readHead < text.Length - 1)
		{
			curCharacterDelay -= Time.deltaTime * (Input.GetMouseButton(0) ? 3f : 1f);

			if(curCharacterDelay < 0)
			{
				curCharacterDelay = characterDelay;
				readHead++;
			}
		}

		if(timeSinceCharCreation[text.Length - 1] < 1f)
		{
			var tempString = new StringBuilder();
			//int fontSize = guiText.fontSize;
			//int fontsize = label.font.size;

			int i = readHead;
			for(;i >= 0;i--)
			{
				timeSinceCharCreation[i] = timeSinceCharCreation[i] + Time.deltaTime;
				// Debug.Log(text[i] + " : " + timeSinceCharCreation[i]);


				// int pixelSize = Mathf.RoundToInt(fontSize * Easing.EaseOut(timeSinceCharCreation[i], EaseType.Bounce));
//				int alpha = 255 * Easing.EaseOut(Mathf.Clamp01(timeSinceCharCreation[i] / bounceTime), EaseType.Bounce);
				//int pixelSize = Mathf.CeilToInt(fontSize * Easing.EaseOut(Mathf.Clamp01(timeSinceCharCreation[i] / bounceTime), EaseType.Bounce));

//				if(!Char.IsWhiteSpace(text[i]))
//				{
//					tempString.Insert(0, String.Format("[{0}]{1}[-]", alpha, text[i]));
//				}
//				else
//				{
//					tempString.Insert(0, text[i]);
//				}
				
				tempString.Insert(0, text[i]);

				if(timeSinceCharCreation[i] >= 1f)
					break;
			}

			if(i >= 0)
				label.text = text.Substring(0, i) + tempString.ToString();
				//guiText.text = text.Substring(0, i) + tempString.ToString();
			else
				label.text = tempString.ToString();
				//guiText.text = tempString.ToString();
		}
	}
}
