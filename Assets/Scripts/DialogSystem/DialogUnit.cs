using UnityEngine;
using System.Collections;

[RequireComponent(typeof(TextAnimate))]
public class DialogUnit : MonoBehaviour {
	
	// String ids
	public string id;
	public string[] responseIds;
	
	// The speechbubble object
	public GameObject speechContainer;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void RunDialogUnit() {
		
	}
}
