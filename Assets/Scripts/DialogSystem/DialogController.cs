using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System;

public class DialogController : MonoBehaviour {
	
	public Transform anchorPoint;
	
	// UI elements being controlled
	public GameObject speechPanel;
	public GameObject textInputPanel;
	public GameObject textInputOkButton;
	public GameObject listWidget;
	
	// For generating MC responses
	public GameObject buttonPrefab;
	
	// TODO: recycle these option button objects
	List<GameObject> optionButtons;
	
	// Behaviour and appearance
	public float speechBounceTime;
	
	// System state
	public enum DialogState {
		Disabled,
		Idle,
		Talking,
		Closing,
		Waiting
	}
	
	public DialogState state;
	
	Waiter speechWaiter;		
	
	// Testing
	public string currentLineId = "v1q1";
	
	// Data	
	public TextAsset rulesFile;
	XElement ruleSet;
	DataStore database;
	XElement currentRule;

	// Use this for initialization
	void Start () {
		state = DialogState.Disabled;
		
		// Initialize gameobject references
		speechPanel.SetActive(false);
		textInputPanel.SetActive(false);
		optionButtons = new List<GameObject>();
		database = GetComponent<DataStore>();
		
		// Load our text resources
		ruleSet = XElement.Parse(rulesFile.text);
	}
	
	// Update is called once per frame
	void Update () {
		if (state == DialogState.Idle && currentLineId.Length > 0) {
			DoDialog(currentLineId);
		}
	}
	
	string QueryNextLine() {
		// Go through the rule elements, find the best match
		var lines = ruleSet.Elements("item")
			.Select( item =>
				new { 
					id = (string) item.Attribute("id"), 
					score = CountRuleMatches(item) 
				})
			.OrderByDescending( r => r.id );
		
		return lines.First().id;
	}
	
	int CountRuleMatches(XElement item) {
		return item.Elements("rule").Where( rule => MatchRule(rule) ).Count();
	}
	
	bool MatchRule(XElement rule) {
		string key = (string) rule.Attribute("key");
		string target = (string) rule.Attribute("value");
		string current = database.GetStateValue(key);
		
		double targetVal;
		bool result = false;
		
		if (double.TryParse(target, out targetVal)) {
			// numeric comparison
			double currentVal = double.Parse(current);
			switch ((string) rule.Attribute("type")) {
			case "lt":
				result = currentVal < targetVal ;
				break;
			case "gt":
				result = currentVal > targetVal;
				break;
			case "lte":
				result = currentVal >= targetVal;
				break;
			case "gte":
				result = currentVal >= targetVal;
				break;		
			case "eq":
				result = currentVal == targetVal;
				break;	
			case "neq":
				result = currentVal != targetVal;
				break;
			}
		} else {
			switch ((string) rule.Attribute("type")) {
			case "eq":
				result = current == target;
				break;	
			case "neq":
				result = current != target;
				break;
			}
		}
		
		return result;
	}
	
	public void DoDialog(string id) {
		// Get the current rule element
		currentRule = ruleSet.Elements("item")
			.Where( r => ((string) r.Attribute("id")) == id )
			.Select( r => r )
			.FirstOrDefault();
		
		if (currentRule == null)
			return;
			
		string lineId = (string) currentRule.Element("dialog").Attribute("id");
		
		ShowSpeechBubble(lineId);
		
		XElement response = currentRule.Element("response");
		string responseType = (string) response.Attribute("type");
		
		if (responseType == "mc") {
			var responseIds = response.Elements("choice")
									.Attributes("id")
									.Select( attr => (string) attr );
			ShowMCResponses(responseIds);
		} else if (responseType == "input") {
			string dictId = (string) response.Attribute("dict");
			ShowTextInputField(dictId);
		}
	}
	
	public void ShowSpeechBubble(string lineId) {
		
		state = DialogState.Talking;
		
		speechPanel.SetActive(true);
		
		speechPanel.GetComponentInChildren<TextAnimate>().Say(Localization.Localize(lineId));
		//speechWidget.transform.parent = anchorPoint;
		//speechWidget.transform.localPosition = Vector3.zero;
		
		// animate pop-in
		speechWaiter = Waiters.Interpolate(speechBounceTime, t => {
			float ease = Easing.EaseOut(t, EaseType.Elastic);
			speechPanel.transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, ease);
		}, speechPanel)
		.Then(() => {
			state = DialogState.Waiting; 
		});
	}
	
	public void CloseSpeechBubble() {		
		if (speechPanel.activeSelf && state != DialogState.Closing) {
			
			Destroy(speechWaiter);
			state = DialogState.Closing;
			speechWaiter = Waiters.Interpolate(speechBounceTime, t => {
				float ease = Easing.EaseOut(t, EaseType.Elastic);
				speechPanel.transform.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, ease);
			}, speechPanel)
			.Then(() => {
				state = DialogState.Idle;
				speechPanel.SetActive(false);
			});
		}
	}
	
	public void ShowMCResponses(IEnumerable<string> responseIds) {
		listWidget.SetActive(true);
		GameObject listGrid = listWidget.transform.Find("ListClipPanel/Grid").gameObject;
		
		// Read the list of response options
		foreach (string id in responseIds) {
			GameObject button = NGUITools.AddChild(listGrid, buttonPrefab);
			button.GetComponentInChildren<UILabel>().text = Localization.Localize(id);
			button.GetComponent<ResponseButton>().Click += OnSelectReponse;
			button.GetComponent<ResponseButton>().responseId = id;
			optionButtons.Add(button);
		}
		listGrid.GetComponent<UIGrid>().repositionNow = true;
	}
	
	public void CloseMCResponses() {
		foreach (GameObject button in optionButtons) {
			Destroy(button);
		}
		optionButtons.Clear();
		listWidget.SetActive(false);
	}
	
	public void ShowTextInputField(string dictName) {
		textInputPanel.SetActive(true);
		textInputPanel.GetComponentInChildren<TextInput>().TextSubmitted += OnTextEntered;
	}
	
	public void OnSelectReponse(string response) {
		
		RecordResponse(currentLineId, response);
		
		CloseSpeechBubble();
		textInputPanel.SetActive(false);
		
		CloseMCResponses();
		
		currentLineId = QueryNextLine();
	}
	
	public void OnTextEntered(string text) {
		var matchedIds = database.GetDictionaryMatches(text, "location");
		textInputPanel.SetActive(false);
		ShowMCResponses(matchedIds.ToArray());
	}
	
	void RecordResponse(string dialogId, string response) {
		string responseKey = dialogId + "_resp";
		string playedKey = dialogId + "_played";
		
		// generic updates
		database.SetStateValue(responseKey, response);
		database.SetStateValue(playedKey, "1");
		
		// save any rule-defined updates
		foreach (var update in currentRule.Elements("update")) {
			database.SetStateValue((string)update.Attribute("key"), response);
		}
	}
}
