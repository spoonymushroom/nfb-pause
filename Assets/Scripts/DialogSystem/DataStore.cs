using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.IO;
using System.Linq;
using System.Text;
using System.Globalization;

public class DataStore : MonoBehaviour {
	
	public TextAsset textData;
	public int maxSuggestions;
	
	public enum DictionaryType {
		Location,
		State,
		Action
	}
	
	// Static data
	Dictionary<string, string> locationDict;	
	Dictionary<string, string> gameState;

	// Use this for initialization
	void Start () {
		LoadDictionaries();	
		
		gameState = new Dictionary<string, string>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void LoadDictionaries () {
		// Load the entries from the localization file into memory to create a searchable dictionary.
		// Oops, this doesn't traverse the XML file properly, but it's good for test for now.
		// TODO: handle synonyms
		locationDict = new Dictionary<string, string>();
		using (XmlReader reader = XmlReader.Create(new StringReader(textData.text))) {
			reader.ReadToFollowing("dictionary");
			if (reader.GetAttribute("name") == "locations") {
				while (reader.ReadToFollowing("item")) {
					string key = reader.GetAttribute("key");
					string val = RemoveDiacritics(Localization.Localize(key));
					locationDict.Add(key, val);
				}
			}
		}		
		
	}
	
	public Dictionary<string, string> GetLocationDictionary() {
		return locationDict;
	}
	
	/// <summary>
	/// Matches the specified text against a dictionary, and returns a list of suggestions.
	/// </summary>
	/// <returns>
	/// The string ids of the suggestions.
	/// </returns>
	/// <param name='text'>
	/// The search text.
	/// </param>
	/// <param name='dictName'>
	/// Name of the dictionary. Can be "location", "action", "state".
	/// </param>
	public IEnumerable<string> GetDictionaryMatches(string text, string dictName) {
		Dictionary<string, string> source;
		switch (dictName) {
		case "location":
			source = locationDict;
			break;
		default:
			source = null;
			break;
		}
		
		// First, try to match against the entries that contain the search string
		var matches = source.Where( entry => entry.Value.IndexOf(text, StringComparison.InvariantCultureIgnoreCase) >= 0 )
						.OrderBy( entry => ComputeStringDistance(entry.Value, text) );
		
		// If no matches, try to match against the entire dictionary
		if (matches.Count() == 0) {
			matches = source.OrderBy( entry => ComputeStringDistance(entry.Value, text) );
		}
		
		return matches.Select( entry => entry.Key ).Take(maxSuggestions);
	}
	
	public string GetStateValue(string key) {
		// TODO: special cases for real-time data, e.g. time, weather, location
		return gameState[key];
	}
	
	public void SetStateValue(string key, string value) {
		gameState[key] = value;
	}
	
	/// <summary>
	/// Strips accents from a string.
	/// </summary>
	/// <returns>
	/// An ascii-compatible string
	/// </returns>
	/// <param name='text'>
	/// The text.
	/// </param>
	static string RemoveDiacritics(string text) {
		var normalizedString = text.Normalize(NormalizationForm.FormD);
	    var stringBuilder = new StringBuilder();
	
	    foreach (var c in normalizedString) {
	        var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
	        if (unicodeCategory != UnicodeCategory.NonSpacingMark) {
	            stringBuilder.Append(c);
	        }
	    }
	
	    return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
	}
	
	/// <summary>
    /// Compute the Levenshtein distance between two strings.
    /// </summary>
    public static int ComputeStringDistance(string s, string t)
    {
		s = s.ToLower();
		t = t.ToLower();
		
		int n = s.Length;
		int m = t.Length;
		int[,] d = new int[n + 1, m + 1];
	
		// Step 1
		if (n == 0) {
		    return m;
		}
	
		if (m == 0) {
		    return n;
		}
	
		// Step 2
		for (int i = 0; i <= n; d[i, 0] = i++) {
		}
	
		for (int j = 0; j <= m; d[0, j] = j++) {
		}
	
		// Step 3
		for (int i = 1; i <= n; i++) {
		    //Step 4
		    for (int j = 1; j <= m; j++) {
				// Step 5
				int cost = (t[j - 1] == s[i - 1]) ? 0 : 1;
		
				// Step 6
				d[i, j] = Math.Min(
				    Math.Min(d[i - 1, j] + 1, d[i, j - 1] + 1),
				    d[i - 1, j - 1] + cost);
		    }
		}
		// Step 7
		return d[n, m];
    }
}
