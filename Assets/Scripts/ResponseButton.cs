using UnityEngine;
using System.Collections;

[RequireComponent(typeof(UIButton))]
public class ResponseButton : MonoBehaviour {
	
	public string responseId;
	public event System.Action<string> Click;
	
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnClick() {
		System.EventArgs args = new System.EventArgs();
		
		// Just to be safe, take a snapshot in case
		// the delegate goes out of scope
		var clickEvent = Click;
		if (clickEvent != null)
			clickEvent(responseId);
	}
}
